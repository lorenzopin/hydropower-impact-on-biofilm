#'pairwise ANOVA
#'
#'This function performs ANOVA of a given measure in specified groups, in addition it
#'also performs pairwise ANOVA of the measure between possible pairs of levels in the grouping variable. It returns
#'p-values obtained.
#'
#' @param df (Required). A \code{data.frame} containg measures a measure to be analysed.
#' @param meta_table (Required). A data frame containing atleast one variable (grouping variable).
#' @param grouping_column (Required). A character string specifying name of the grouping variable in the supplied meta table.
#' @return It returns a list of two data frames: One being p-values obtained from the pairwise ANOVA of
#' the measure and levels of grouping variable , the other is containing updated measure.
#' @references \url{http://userweb.eng.gla.ac.uk/umer.ijaz/}, Umer Ijaz, 2015
#'
#' @author Alfred Ssekagiri \email{assekagiri@gmail.com},Umer Zeeshan Ijaz \email{Umer.Ijaz@glasgow.ac.uk}
#'
#' @import data.table
#'
#' @export perform_anova
#'

perform_anova <- function(df,meta_table,grouping_column,pValueCutoff){
  
  dt<-data.table::data.table(data.frame(df,.group.=meta_table[,grouping_column]))
  #specifying a p-value cutoff for the ggplot2 strips
  pval<-dt[, list(pvalue = sprintf("%.2g",
                                   tryCatch(summary(aov(value ~ .group.))[[1]][["Pr(>F)"]][1],error=function(e) NULL))),
           by=list(measure)]
  #Filter out pvals that we are not significant
  pval<-pval[!pval$pvalue=="",]
  pval<-pval[as.numeric(pval$pvalue)<=pValueCutoff,]
  
  #using sapply to generate significances for pval$pvalue using the cut function.
  pval$pvalue<-sapply(as.numeric(pval$pvalue),function(x){as.character(cut(x,breaks=c(-Inf, 0.001, 0.01, pValueCutoff, Inf),label=c("***", "**", "*", "")))})
  
  #Update df$measure to change the measure names if the grouping_column has more than three classes
  if(length(unique(as.character(meta_table[,grouping_column])))>2){
    df$measure<-as.character(df$measure)
    if(dim(pval)[1]>0){
      for(i in seq(1:dim(pval)[1])){
        df[df$measure==as.character(pval[i,measure]),"measure"]=paste(as.character(pval[i,measure]),as.character(pval[i,pvalue]))
      }
    }
    df$measure<-as.factor(df$measure)
  }
  #Get all possible pairwise combination of values in the grouping_column
  s<-combn(unique(as.character(df[,grouping_column])),2)
  
  #df_pw will store the pair-wise p-values
  df_pw<-NULL
  for(k in unique(as.character(df$measure))){
    #We need to calculate the coordinate to draw pair-wise significance lines
    #for this we calculate bas as the maximum value
    bas<-max(df[(df$measure==k),"value"])
    #Calculate increments as 10% of the maximum values
    inc<-0.1*bas
    #Give an initial increment
    bas<-bas+inc
    for(l in 1:dim(s)[2]){
      #Do a pair-wise anova
      tmp<-c(k,s[1,l],s[2,l],bas,paste(sprintf("%.2g",tryCatch(summary(aov(as.formula(paste("value ~",grouping_column)),data=df[(df$measure==k) & (df[,grouping_column]==s[1,l] | df[,grouping_column]==s[2,l]),] ))[[1]][["Pr(>F)"]][1],error=function(e) NULL)),"",sep=""))
      #Ignore if anova fails
      if(!is.na(as.numeric(tmp[length(tmp)]))){
        #Only retain those pairs where the p-values are significant
        if(as.numeric(tmp[length(tmp)])<pValueCutoff){
          if(is.null(df_pw)){df_pw<-tmp}else{df_pw<-rbind(df_pw,tmp)}
          #Generate the next position
          bas<-bas+inc
        }
      }
    }
  }
  if(!is.null(df_pw)){
    df_pw<-data.frame(row.names=NULL,df_pw)
    names(df_pw)<-c("measure","from","to","y","p")
  }
  out <- list("df_pw"=df_pw, "df"=df)
  return(out)
}





#'ANOVA of environmental variables
#'
#'This functions applies analysis of variance on the measured environmental variables
#'in specified groups and constructs a plot showing how the environmental variables vary in the groups.
#'The plot is annotated with significance level as obtained from \code{anova}.
#'
#' @param physeq (Required). A \code{phyloseq} object containing merged information of abundance,
#'        taxonomic assignment, sample data including the measured variables and categorical information
#'        of the samples, and / or phylogenetic tree if available.
#' @param grouping_column (Required). Name of a categorical variable that is preffered for grouping the.
#'        information, this should be one of the components of grouping vector.
#' @param pValueCutoff. p-value threshold for significance of anova results, default set to 0.05.
#' @param select.variables. A vector of character strings(s) specifying environmental variable(s) to be analysed. If
#' not supplied, all numeric variables are analysed.
#' @return Returns a ggplot object. This can further be manipulated as preferred by user.
#' @examples
#' data(pitlatrine)
#' physeq<-pitlatrine
#' p1<-plot_anova_env(physeq,grouping_column =  "Country",select.variables=c("Temp","pH"))
#' print(p1)
#'
#' @references \url{http://userweb.eng.gla.ac.uk/umer.ijaz/}, Umer Ijaz, 2015
#'
#' @author Alfred Ssekagiri \email{assekagiri@gmail.com}, Umer Zeeshan Ijaz \email{Umer.Ijaz@glasgow.ac.uk}
#'
#' @export plot_anova_env
#'
plot_anova_env <- function(physeq, grouping_column, pValueCutoff=0.05,select.variables=NULL){
  
  #get meta data from phyloseq object
  meta_table <- as.data.frame(sample_data(physeq))
  #pick numerical variables of environmental data
  env_table <- meta_table[,sapply(meta_table,is.numeric)]
  df<- reshape2::melt(as.matrix(env_table))
  names(df)<-c("sample","measure","value")
  #Incorporate categorical data in df
  df<-data.frame(df,(meta_table[, grouping_column])[as.character(df$sample),])
  
  #do anova of environmental variables between groups
  anova_res <- perform_anova(df,meta_table,grouping_column,pValueCutoff)
  df_pw <- anova_res$df_pw #get pairwise p-values
  df <- anova_res$df #get updated environmental measure information
  
  #pick selected variables
  if(!is.null(select.variables)){
    df <- df[which(df$measure%in%select.variables),]
    df_pw<-df_pw[which(df_pw$measure%in%select.variables),]
  }
  #Draw the boxplots
  p<-ggplot2::ggplot(aes_string(x=grouping_column,y="value",color=grouping_column),data=df)
  p<-p+ggplot2::geom_boxplot()+geom_jitter(position = position_jitter(height = 0, width=0))
  p<-p+ggplot2::theme_bw()+geom_point(size=1,alpha=0.2)+theme(axis.text.x = element_text(angle = 90, hjust = 1))
  p<-p+ggplot2::facet_wrap(~measure,scales="free_y",nrow=1)+ylab("Observed Values")+xlab("Groups")
  p<-p+ggplot2::theme(strip.background = element_rect(fill = "white"))
  #This loop will generate the lines and signficances
  for(i in 1:dim(df_pw)[1]){
    p<-p+ ggplot2::geom_path(inherit.aes=F,aes(x,y),data = data.frame(x = c(which(levels(df[,grouping_column])==as.character(df_pw[i,"from"])),which(levels(df[,grouping_column])==as.character(df_pw[i,"to"]))), y = c(as.numeric(as.character(df_pw[i,"y"])),as.numeric(as.character(df_pw[i,"y"]))), measure=c(as.character(df_pw[i,"measure"]),as.character(df_pw[i,"measure"]))), color="black",lineend = "butt",arrow = arrow(angle = 90, ends = "both", length = unit(0.1, "inches")))
    p<-p+ ggplot2::geom_text(inherit.aes=F,aes(x=x,y=y,label=label),data=data.frame(x=(which(levels(df[,grouping_column])==as.character(df_pw[i,"from"]))+which(levels(df[,grouping_column])==as.character(df_pw[i,"to"])))/2,y=as.numeric(as.character(df_pw[i,"y"])),measure=as.character(df_pw[i,"measure"]),label=as.character(cut(as.numeric(as.character(df_pw[i,"p"])),breaks=c(-Inf, 0.001, 0.01, 0.05, Inf),label=c("***", "**", "*", "")))))
  }
  return(p)
}




#' Canonical Correspondence Analysis plot
#'
#' This function finds a set of best environmental variables that describe community structure
#' superimposes them on the ordination plot.
#'
#' @param `physeq` is a required phyloseq object containing taxa abundance and meta data.
#' @param `grouping_column` is the variable in the meta data with respect to which the data should be grouped,
#' @param `pvalueCutoff` the threshold p-value in `anova` of distance matrices, default set to `0.05`.
#' @param `env.variables` is a list of variables prefered to be on the cca plot.
#' @param `exclude.variables` a list of variables to be excluded from the cca plot.
#' @param `num.env.variables` is an integer specifying the number of variables to show on the cca plot.
#'
#' @return A ggplot object
#'
#' @examples
#' data(pitlatrine)
#' physeq<-data(physeq)
#' ccaplot  <- plot_cca(physeq, grouping_column = "Country")
#'
#' @references \url{http://userweb.eng.gla.ac.uk/umer.ijaz/}, Umer Ijaz, 2015
#'
#' @author Alfred Ssekagiri \email{assekagiri@gmail.com},  Umer Zeeshan Ijaz \email{Umer.Ijaz@glasgow.ac.uk}
#'
#' @export plot_cca

plot_cca <- function(physeq, grouping_column, pvalueCutoff=0.01,norm_method=NULL, env.variables=NULL,
                     num.env.variables=NULL, exclude.variables=NULL, draw_species=F){
  #extract abundance and meta data from supplied phyloseq object
  abund_table <- otu_table(physeq)
  meta_table <- data.frame(sample_data(physeq))
  
  #Use adonis to find significant environmental variables
  abund_table.adonis <- vegan::adonis(abund_table ~ ., data=meta_table)
  #pick significant features
  bestEnvVariables<-rownames(abund_table.adonis$aov.tab)[abund_table.adonis$aov.tab$"Pr(>F)"<=pvalueCutoff]
  #throw out na in case any exist
  bestEnvVariables<-bestEnvVariables[!is.na(bestEnvVariables)]
  #pick out the selected variables if at all they are part of the significant ones
  if(!is.null(env.variables)&&(env.variables%in%bestEnvVariables)){
    bestEnvVariables <- env.variables
  }
  #provide number of variables to display
  if(!is.null(num.env.variables)){
    if(num.env.variables>length(bestEnvVariables)){
      stop(cat(paste("Choose a number less than",length(bestEnvVariables))))
    }else{
      bestEnvVariables <- bestEnvVariables[1:num.env.variables]
    }
  }
  #exclude selected variables from appearing on the plot
  if(!is.null(exclude.variables)&&(exclude.variables%in%bestEnvVariables)){
    bestEnvVariables <- bestEnvVariables[!(bestEnvVariables%in%exclude.variables)]
  }
  #We are now going to use only those environmental variables in cca that were found significant
  eval(parse(text=paste("sol <- cca(abund_table ~ ",do.call(paste,c(as.list(bestEnvVariables),sep=" + ")),",data=meta_table)",sep="")))
  
  scrs<- vegan::scores(sol,display=c("sp","wa","lc","bp","cn"))
  #Extract site data first
  df_sites<-data.frame(scrs$sites,meta_table[,grouping_column])
  colnames(df_sites)<-c("x","y","Groups")
  
  #Draw sites
  p<-ggplot2::ggplot()
  p<-p+ggplot2::geom_point(data=df_sites,aes(x,y,colour=Groups))
  #Draw biplots
  multiplier <- vegan:::ordiArrowMul(scrs$biplot)
  df_arrows<- scrs$biplot*multiplier
  colnames(df_arrows)<-c("x","y")
  df_arrows=as.data.frame(df_arrows)
  p<-p+geom_segment(data=df_arrows, aes(x = 0, y = 0, xend = x, yend = y),arrow = arrow(length = unit(0.2, "cm")),color="#808080",alpha=0.5)
  p<-p+geom_text(data=as.data.frame(df_arrows*1.1),aes(x, y, label = rownames(df_arrows)),color="#808080",alpha=0.5)
  # Draw species
  df_species<- as.data.frame(scrs$species)
  colnames(df_species)<-c("x","y")
  # Either choose text or points
  #p<-p+geom_text(data=df_species,aes(x,y,label=rownames(df_species)))
  if(draw_species){
    p<-p+geom_point(data=df_species,aes(x,y,shape="Species"))+scale_shape_manual("",values=2)
  }
  p<-p+theme_bw()+xlab("CCA1")+ylab("CCA2")
  return(p)
}




plot_taxa_boxplot <- function(x, taxonomic.level,
                              top.otu,
                              keep.other = FALSE,
                              group,
                              title,
                              group.colors = NULL,
                              group.order = NULL,
                              add.violin = TRUE,
                              violin.opacity = 0.25,
                              box.opacity = 0.25,
                              dot.opacity = 0.25,
                              dot.size = 2) {
  Abundance <- Taxa <- RelAbun <- NULL
  if (!is.null(x@phy_tree)) {
    message("For plotting purpuses the phy_tree will be removed")
    x@phy_tree <- NULL
  }
  else {
    message("The phy_tree slot is empty, easy to make the plot")
  }
  
  
  # taxic <- as.data.frame(x@tax_table)
  taxic <- tax_table(x) %>%
    as("matrix") %>%
    as.data.frame()
  # using abundances function from microbiome as sometime otu_table can have taxa_are_rows FALSE in phyloseq
  # otudf <- as.data.frame(abundances(x)
  otudf2 <- abundances(x) %>%
    as("matrix") %>%
    as.data.frame()
  taxic$OTU <- row.names(otudf2)
  taxmat <- as.matrix(taxic)
  new.tax <- tax_table(taxmat)
  tax_table(x) <- new.tax
  
  
  # Merge the taxa at a higher taxonomic level
  
  if (!taxonomic.level == "OTU") {
    x <- aggregate_top_taxa2(x, taxonomic.level, top = top.otu)
  }
  
  x1 <- transform(x, "compositional")
  
  x.df0 <- suppressWarnings(suppressMessages(phy_to_ldf(x1, transform.counts = NULL)))
  # x.df0$RelAbun <- as.numeric(x.df0$Abundance * 100)
  x.df0$Taxa <- x.df0[, taxonomic.level]
  
  if (keep.other == FALSE) {
    x.df0 <- subset(x.df0, Taxa != "Other")
  }
  if (!is.null(group.order)) {
    x.df0[, group] <- factor(x.df0[, group],
                             levels = group.order
    )
  }
  
  p <- ggplot(x.df0, aes(
    x = x.df0[, group],
    y = Abundance,
    fill = x.df0[, group]
  ))
  
  p <- p + geom_boxplot(aes_string(fill = group),
                        width = 0.2,
                        outlier.shape = NA,
                        alpha = box.opacity
  ) +
    geom_jitter(aes_string(
      group = x.df0[, group],
      color = group
    ),
    alpha = dot.opacity, size = dot.size
    )
  if (add.violin == TRUE) {
    p <- p + geom_half_violin(
      position = position_nudge(x = 0.15, y = 0),
      alpha = violin.opacity, side = "r"
    )
  }
  p <- p + ggtitle(title) + theme_bw() +
    facet_wrap(~Taxa, scales = "free")
  
  p <- p + ylab("Relative abundance (%)") + xlab(taxonomic.level) +
    scale_fill_manual(group,
                      values = group.colors
    ) +
    scale_color_manual(group,
                       values = group.colors
    ) +
    scale_y_continuous(labels = scales::percent)
  
  return(p + xlab(""))
}







#' @title Taxa Names
#' @description List the names of taxonomic groups in a phyloseq object.
#' @inheritParams core_members
#' @return A vector with taxon names.
#' @details A handy shortcut for phyloseq::taxa_names, with a potential to add
#' to add some extra tweaks later.
#' @references 
#' To cite the microbiome R package, see citation('microbiome') 
#' @author Contact: Leo Lahti \email{microbiome-admin@@googlegroups.com}
#' @keywords utilities
#' @export
#' @examples
#' data(dietswap)
#' x <- taxa(dietswap)

taxa <- function(x) {
  
  if (taxa_are_rows(x)) {
    rownames(otu_table(x))    
  } else {
    colnames(otu_table(x))    
  }  
}







#' @title Retrieve Phyloseq Metadata as Data Frame
#' @description The output of the phyloseq::sample_data() function does not
#' return data.frame, which is needed for many applications.
#' This function retrieves the sample data as a data.frame
#' @param x a phyloseq object
#' @return Sample metadata as a data.frame
#' @export
#' @author Leo Lahti \email{leo.lahti@@iki.fi}
#' @examples data(dietswap); df <- meta(dietswap)
#' @seealso \code{\link{sample_data}} in the \pkg{phyloseq} package
#' @keywords utilities
meta <- function(x) {
  df <- as(sample_data(x), "data.frame")
  rownames(df) <- sample_names(x)
  df
}







#' @title Abundance Matrix from Phyloseq
#' @description Retrieves the taxon abundance table from
#' phyloseq-class object and ensures it is systematically returned as
#' taxa x samples matrix.
#' @inheritParams transform
#' @return Abundance matrix (OTU x samples).
#' @references See citation('microbiome') 
#' @author Contact: Leo Lahti \email{microbiome-admin@@googlegroups.com}
#' @export
#' @aliases ab, otu
#' @examples
#' data(dietswap)
#' a <- abundances(dietswap)
#' # b <- abundances(dietswap, transform='compositional')
#' @keywords utilities
abundances <- function(x, transform="identity") {
  
  # Pick the OTU data
  if (length(is(x)) == 1 && "phyloseq" %in% is(x)) {
    
    # Pick OTU matrix
    otu <- as(otu_table(x), "matrix") # get_taxa(x)
    
    # Ensure that taxa are on the rows
    if (!taxa_are_rows(x) && ntaxa(x) > 1 && nsamples(x) > 1) {
      otu <- t(otu)
    }
    
    if (ntaxa(x) == 1) {
      otu <- matrix(otu, nrow=1)
      rownames(otu) <- taxa(x)
      colnames(otu) <- sample_names(x)
    }
    
    if (nsamples(x) == 1) {
      otu <- matrix(otu, ncol=1)
      rownames(otu) <- taxa(x)
      colnames(otu) <- sample_names(x)
    }
    
  } else if (is.vector(x)) {
    
    otu <- as.matrix(x, ncol=1)
    
  } else {
    
    # If x is not vector or phyloseq object then let us assume it is a
    # taxa x samples
    # count matrix
    otu <- x
    
  }
  
  # Apply the indicated transformation
  if (!transform == "identity") {
    otu <- transform(otu, transform)
  }
  otu
  
}


#' @title Abundance Boxplot
#' @description Plot phyloseq abundances.
#' @param d \code{\link{phyloseq-class}} object
#' @param x Metadata variable to map to the horizontal axis.
#' @param y OTU to map on the vertical axis
#' @param line The variable to map on lines
#' @param violin Use violin version of the boxplot
#' @param na.rm Remove NAs
#' @param show.points Include data points in the figure
#' @details The directionality of change in paired boxplot is indicated by
#' the colors of the connecting lines.
#' @return A \code{\link{ggplot}} plot object
#' @export
#' @examples
#' data(peerj32)
#' p <- boxplot_abundance(peerj32$phyloseq, x='time', y='Akkermansia',
#'    line='subject')
#' @keywords utilities
boxplot_abundance <-
  function(d, x, y, line=NULL, violin=FALSE, na.rm=FALSE, show.points=TRUE) {
    
    change <- xvar <- yvar <- linevar <- colorvar <- NULL
    pseq <- d
    
   otu <- abundances(pseq)
     # otu <- abundances(pseq) if (!taxa_are_rows(pseq)) {otu <- t(otu)}
    
    # Construct example data (df). Ensure that samples are given in same order
    # in metadata and HITChip data.  FIXME: this can be removed when official
    # phyloseq package is fixed so as to retain the factor level ordering
    
    df <- meta(pseq)
    
    df$xvar <- df[[x]]
    if (!is.factor(df[[x]])) {
      df$xvar <- factor(as.character(df$xvar))
    }
    
    if (y %in% taxa(pseq)) {
      df$yvar <- as.vector(unlist(otu[y, ]))
    } else {
      df$yvar <- as.vector(unlist(sample_data(pseq)[, y]))
    }
    
    if (na.rm) {
      df <- subset(df, !is.na(xvar))
      df <- subset(df, !is.na(yvar))
    }
    
    if (nrow(df) == 0) {
      warning("No sufficient data for plotting available. 
            Returning an empty plot.")
      return(ggplot())
    }
    
    # Factorize the group variable
    df$xvar <- factor(df$xvar)
    
    # Visualize example data with a boxplot
    p <- ggplot(df, aes(x=xvar, y=yvar))
    
    if (show.points) {
      p <- p + geom_point(size=2,
                          position=position_jitter(width=0.3), alpha=0.5)
    }
    
    # Box or Violin plot ?
    if (!violin) {
      p <- p + geom_boxplot(fill=NA)
    } else {
      p <- p + geom_violin(fill=NA)
    }
    
    # Add also subjects as lines and points
    if (!is.null(line)) {
      df$linevar <- factor(df[[line]])
      
      # Calculate change directionality
      df2 <- suppressWarnings(df %>%
                                arrange(linevar, xvar) %>%
                                group_by(linevar) %>% 
                                summarise(change=diff(yvar)))
      
      # Map back to data
      df$change <- df2$change[match(df$linevar, df2$linevar)]
      
      # Only show the sign of change for clarity
      df$change <- sign(df$change)
      p <- p + geom_line(data=df,
                         aes(group=linevar, color=change), size=1) +
        scale_colour_gradient2(low="blue", mid="black", high="red", 
                               midpoint=0, na.value="grey50", guide="none")
    }
    #if (!is.null(color)) {   
    #    df$colorvar <- factor(df[[color]])
    #    # Add legend
    #    # p <- p + geom_point(data=df, aes(color=colorvar), size=4)
    #    # label p <- p + guides(color=guide_legend(title=color))
    #}
    
    # Add axis tests
    p <- p + xlab(x) + ylab(y)
    
    # Return ggplot object
    p
    
  }


